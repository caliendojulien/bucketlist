<?php

namespace App\Form;

use App\Entity\Category;
use App\Entity\Wish;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class WishType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', null, [
                "label" => "Titre : ",
                "attr" => [
                    "class" => "w-full max-w-xs"
                ]
            ])
            ->add('auteur', null, [
                "label" => "Auteur : ",
                "label_attr" => [
                    "class" => "label-text"
                ],
                "attr" => [
                    "class" => "w-full max-w-xs"
                ]
            ])
            ->add('description', null, [
                "attr" => [
                    "class" => "w-full max-w-xs"
                ]
            ])
            ->add('categorie', EntityType::class, [
                "class" => Category::class,
                "choice_label" => 'name'
            ])
            ->add('Ajouter', SubmitType::class, [
                "attr" => [
                    "class" => "btn btn-ghost"
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Wish::class,
        ]);
    }
}
